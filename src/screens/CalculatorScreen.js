import React, {useState, useRef} from "react";
import {View, Text, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import CalculatorButtonView from "../components/CalculatorButtonView";

const CalcScreen = () => {
    const [mathematicalExpression, setMathematicalExpression] = useState('');
    const [score, setScore] = useState('');
    const [history, setHistory] = useState('');

    const penultimateCharacter = mathematicalExpression.charAt(mathematicalExpression.length - 2);
    const lastCharacter = mathematicalExpression.charAt(mathematicalExpression.length - 1);

    const scrollToEnd = useRef(null);

    const buttonPress = (value) => {
        setScore('');
        var number = mathematicalExpression.split(/([-+*/])/);
        lastNumber = number.pop();
        if (value == "%" && mathematicalExpression.length > 0) {
            strLastNumber = lastNumber.length;
            if (mathematicalExpression.substring(0, mathematicalExpression.length-strLastNumber).slice(-1) == "+"
            || mathematicalExpression.substring(0, mathematicalExpression.length-strLastNumber).slice(-1) == "-") {
                lastNumber = lastNumber/100;

                toPercent = mathematicalExpression.substring(0, mathematicalExpression.length-strLastNumber);
                
                let count;
                const isSignAtTheEnd = isNaN(toPercent.slice(-1));
                if (isSignAtTheEnd == true) {
                    if (isNaN(toPercent.charAt(toPercent.length-2))) {
                        toPercent = toPercent.substring(0, toPercent.length-2);
                    } else {
                        toPercent = toPercent.substring(0, toPercent.length-1);
                    }
                } else {
                    count = eval(mathematicalExpression);
                }
                count = eval(toPercent);
                count = count*lastNumber;

                setMathematicalExpression(mathematicalExpression.substring(0,mathematicalExpression.length-strLastNumber) + count);
                calculateString(mathematicalExpression.substring(0,mathematicalExpression.length-strLastNumber) + count);
            } else {
                count = lastNumber*0.01;
                setMathematicalExpression(mathematicalExpression.substr(0, mathematicalExpression.length-strLastNumber) + count);

            } 
            

            
            console.log('0')
        } else if ((mathematicalExpression.length == 0 && value != '-' && isNaN(value))) {
            console.log('1')
        } else if (isNaN(penultimateCharacter) && isNaN(lastCharacter) && isNaN(value)) {
            setMathematicalExpression(mathematicalExpression);
            calculateString(mathematicalExpression.substring(0, mathematicalExpression.length-2));
            console.log('2')
        } else if (lastCharacter == "-" && value == "-") {
            setMathematicalExpression(mathematicalExpression);
            if (isNaN(lastCharacter)) {
                calculateString(mathematicalExpression.substring(0,mathematicalExpression.length-1));
            } else {
                calculateString(mathematicalExpression);
            }
            console.log('3')
        } else if (lastCharacter == '.' && value == '-') {
            setMathematicalExpression(mathematicalExpression.replace(/.$/, value));
            console.log('4')
        } else if (lastNumber.indexOf(".") > 0 && value == '.') {
            calculateString(mathematicalExpression)
            console.log('5')
        } else if (isNaN(lastCharacter) && isNaN(value) && value != "-") {
            if (value == '.') {
                calculateString(mathematicalExpression)
            } else {
                setMathematicalExpression(mathematicalExpression.replace(/.$/, value));
                calculateString(mathematicalExpression);
            }
            
            console.log('6')
        } else if (lastNumber == "0" && !isNaN(value)) {
            calculateString(mathematicalExpression);
            console.log('7');
        } else {
            setMathematicalExpression(mathematicalExpression+value);
            if (!isNaN(value)) {
                calculateString(mathematicalExpression+value);
            } else {
                if (isNaN(lastCharacter) && isNaN(value)) {
                    calculateString(mathematicalExpression.substring(0,mathematicalExpression.length-1))
                } else {
                    calculateString(mathematicalExpression);
                }
            }
            console.log('8')
        }
    }

    const clear = () => {
        setMathematicalExpression('');
        setScore('');
    }

    const deleteLastCharacter = () => {
        if (mathematicalExpression.length > 0) {
            setMathematicalExpression(mathematicalExpression.replace(/.$/, ''));
            setScore(mathematicalExpression.replace(/.$/, ''));
            if (isNaN(mathematicalExpression.charAt(mathematicalExpression.length - 2)) == true) {
                if (isNaN(mathematicalExpression.charAt(mathematicalExpression.length - 3)) == true) {
                    calculateString(mathematicalExpression.substring(0, mathematicalExpression.length-3));
                } else {
                    calculateString(mathematicalExpression.substring(0, mathematicalExpression.length-2));
                }
            } else {
                calculateString(mathematicalExpression.substring(0, mathematicalExpression.length-1));
            }
        }
    }

    const calculate = () => {
        console.log(mathematicalExpression.length)
        if (isNaN(mathematicalExpression.charAt(mathematicalExpression.length - 1))) {
            if (isNaN(mathematicalExpression.charAt(mathematicalExpression.length - 2))) {
                setHistory(history + "\n" + mathematicalExpression.substring(0, mathematicalExpression.length - 2) + "=" + score);
            } else {
                setHistory(history + "\n" + mathematicalExpression.replace(/.$/, '') + "=" + score);
            }
           
        } else {
            setHistory(history + "\n" + mathematicalExpression + "=" + score);
        }

        setMathematicalExpression('' + score);
        setScore('');

        // setTimeout(scrollToEnd.current.scrollToEnd(), 500);

        scrollToEnd.current.scrollToEnd();
        

    }

    function calculateString(mathematicalExpression){
        let count;
        const isSignAtTheEnd = isNaN(mathematicalExpression.slice(-1));
        if (isSignAtTheEnd == true) {
            mathematicalExpression = mathematicalExpression.replace(/.$/, '');
            count = eval(mathematicalExpression);
        } else {
            count = eval(mathematicalExpression);
        }
        setScore(count); 

    }


 


    return (
        <View >
        <View style={style.operationBlock}>
            
            <ScrollView ref={scrollToEnd}>
            <Text style={{textAlign: "right", fontSize: 25, color: "white"}}>
                {history}
            </Text>
            </ScrollView>
        </View>
        <View style={style.expressionBlock}>
            <Text style={{textAlign: "right", fontSize: 50, color: "white"}}>
                {mathematicalExpression}
            </Text>
        </View>
        <View style={style.resultBlock}>
            <Text style={{textAlign: "right", fontSize: 25, color: "white"}}>
                {score}
            </Text>
        </View>
        <View style={style.container}>
            <View style={style.row}>
            <CalculatorButtonView sign='AC' digitalOrSign='sign' buttonPress={clear} />
            <CalculatorButtonView sign='D' digitalOrSign='sign' buttonPress={deleteLastCharacter} />
            <CalculatorButtonView sign='%' digitalOrSign='sign' buttonPress={buttonPress} />
            <CalculatorButtonView sign='/' digitalOrSign='sign' buttonPress={buttonPress} />
            </View>
            <View style={style.row}>
            <CalculatorButtonView sign='7' digitalOrSign='digital' buttonPress={buttonPress}  />
            <CalculatorButtonView sign='8' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='9' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='*' digitalOrSign='sign' buttonPress={buttonPress} />
            </View>
            <View style={style.row}>
            <CalculatorButtonView sign='4' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='5' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='6' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='-' digitalOrSign='sign' buttonPress={buttonPress} />
            </View>
            <View style={style.row}>
            <CalculatorButtonView sign='1' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='2' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='3' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='+' digitalOrSign='sign' buttonPress={buttonPress} />
            </View>
           <View style={style.row}>
           <CalculatorButtonView sign='' digitalOrSign='sign' buttonPress={buttonPress} />
            <CalculatorButtonView sign='0' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='.' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='=' digitalOrSign='sign' buttonPress={calculate} />
        </View>
       
        
        </View>

        </View>

    )
}

const style = StyleSheet.create({


    operationBlock: {
        backgroundColor: "black",
        height:"30%",
        alignSelf:"stretch",
        justifyContent: "flex-end"
    },
    expressionBlock: {
        backgroundColor:"black",
        height:"15%",
        alignSelf:"stretch",
        justifyContent: "flex-end",
    },
    resultBlock: {
        backgroundColor:"black",
        height:"5%",
        alignSelf:"stretch",
        fontSize:20,
        justifyContent: "flex-end",
    },
    container: {
        backgroundColor: 'black',
        height:"50%",
    },
    row: {
        flexDirection: 'row',
        flex: 1,
    },
        listStyle: {
        marginTop: 0
        },

    


})

export default CalcScreen;