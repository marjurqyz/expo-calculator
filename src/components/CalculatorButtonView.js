import React, {useState} from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const CalculatorButtonView = props => {
if (props.digitalOrSign == 'digital') {
    return (
        <TouchableOpacity style={style.button}>
            <Text style={style.textStyleNumber} onPress={() => {props.buttonPress(props.sign)}}>
                {props.sign}
            </Text>
        </TouchableOpacity>
    );
} 
else if (props.digitalOrSign == 'sign') {
    return (
        
        <TouchableOpacity style={style.button} onPress={() => {props.buttonPress(props.sign)}}>
            <Text style={style.textStyleOperation}>
                {props.sign}
            </Text>
        </TouchableOpacity>
    );
}
    
}

const style = StyleSheet.create(
    {
        button: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'black',
        },
        textStyleNumber: {
            color:"white",
            fontWeight:"900",
            fontSize:30,
            padding: 30
        },
        textStyleOperation: {
            color:"orange",
            fontWeight:"900",
            fontSize:30,
            padding: 20
        },
    }
)
export default CalculatorButtonView;